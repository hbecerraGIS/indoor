ALTER TABLE "capa" ALTER COLUMN fid TYPE int4

CREATE TABLE public.polygons AS
SELECT
   g.path[1] as fid,
   g.geom::geometry(polygon, 3857) as geom
FROM
   (SELECT
     (ST_Dump(ST_Polygonize(geom))).*
   FROM public.lines
) as g;

ALTER TABLE public.polygons ALTER COLUMN geom
SET DATA TYPE geometry(MultiPolygon) USING ST_MULTI(geom),
ADD COLUMN description VARCHAR (512),
ADD COLUMN short_description VARCHAR (512),
ADD COLUMN weight INTEGER,
ADD COLUMN type VARCHAR (512),
ADD COLUMN subtype VARCHAR (512),
ADD COLUMN path VARCHAR (512),
ADD COLUMN ruqc_id VARCHAR (512),
ADD COLUMN subtype_category VARCHAR (512),
ADD COLUMN area NUMERIC (8,3);

UPDATE public.polygons SET area = ST_Area(geom);
UPDATE public.polygons SET fid = gid;
